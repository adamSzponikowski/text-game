import java.util.Scanner;

class Main {
    static Story start = new Story();

    public static void main(String[] args) {
        start.game();
    }

}


interface EscapeRoom {
    Scanner input = new Scanner(System.in);

    public void menu();

    public void play();
}


class Room1 implements EscapeRoom {
    static Room2 y = new Room2();
    boolean keyFound = false;

    public void youFoundKey() {
        this.keyFound = true;
    }


    public void playerChoice2() {
        System.out.println("1.Take the key");
        System.out.println("2.Leave the key");
        int userChoice2 = input.nextInt();
        if (userChoice2 == 1) {
            youFoundKey();
            System.out.println("You have taken they key");
            play();
        } else {
            System.out.println("You are not taking the key");
            play();
        }
    }

    @Override
    public void menu() {
        System.out.println("1. Show equipment");
        System.out.println("2. Describe current room");
        System.out.println("3. Search the desk");
        System.out.println("4. Search the wardrobe.");
        System.out.println("5. Search the bucket with water");
        System.out.println("6. Go to the next room");
    }

    @Override
    public void play() {
        menu();
        int userChoice1 = input.nextInt();
        if (userChoice1 == 1) {
            if (keyFound == true) {
                System.out.println("You have got a key in your equipment");

            } else {
                System.out.println("At this moment there is nothing to show");

            }
            play();
        }
        if (userChoice1 == 2) {
            System.out.println("It`s dark in here, the room is small and something stinks");
            play();
        }
        if (userChoice1 == 3) {
            System.out.println("There is nothing in it");
            play();
        }
        if (userChoice1 == 4) {
            System.out.println("Sadly, nothing is here");
            play();
        }
        if (userChoice1 == 5) {
            System.out.println("Oh, It was gross but you have found the key");
            playerChoice2();
        }
        if (userChoice1 == 6) {
            if (keyFound == true) {
                System.out.println("You are using the key to open the door, it`s still dark, and there are some paintings on the wall");
                this.keyFound = false;
                y.play();

            } else {
                System.out.println("It`s closed, you cant go in");
                play();
            }
        }
    }

}


class Room2 implements EscapeRoom {
    private boolean noteFound = false;

    private void youFoundNote() {
        this.noteFound = true;
    }


    private void insertPassword() {
        Scanner x = new Scanner(System.in);
        String password = "0,6,2";
        System.out.println("Provide 3 numbers: (Seperate it with comma)");
        String s = x.nextLine();
        if (s.equals(password)) {
            System.out.println("Good Job, you have entered the correct password!");
            System.out.println("Turns out it's your bachelor party, enjoy your party!");
        } else {
            System.out.println("You have entered, the bad password");
            play();
        }
    }


    private void noteDescription() {
        System.out.println("You have taken the note, you can read something from it : ");
        System.out.println("6,8,2");
        System.out.println("6,1,4");
        System.out.println("2,0,6");
        System.out.println("7,3,8");
        System.out.println("7,8,0");
    }


    @Override
    public void menu() {
        System.out.println("1. Show equipment");
        System.out.println("2. Describe current room");
        System.out.println("3. Search the desk");
        System.out.println("4. Search the wardrobe.");
        System.out.println("5. Go to the next room");
    }


    private void roomDescription() {
        System.out.println("There are some sentences on the wall");
        System.out.println("First one : One correct number is on the correct place");
        System.out.println("Second one : One correct number is on the wrong place");
        System.out.println("Third one : Two numbers are good, both on bad place");
        System.out.println("Fourth one : All the numbers are bad");
        System.out.println("Fifth one : One good number on the bad place");
    }


    private void playerChoice() {
        System.out.println("1.Take the note");
        System.out.println("2.Leave the note");
        int userChoice3 = input.nextInt();
        if (userChoice3 == 1) {
            youFoundNote();
            noteDescription();
            play();
        } else {
            System.out.println("You are not taking the note");
            play();
        }
    }


    @Override
    public void play() {
        menu();
        int userChoice4 = input.nextInt();
        if (userChoice4 == 1) {
            if (noteFound == true) {
                noteDescription();
                play();
            } else {
                System.out.println("There is nothing to show");
                play();
            }
        }
        if (userChoice4 == 2) {
            roomDescription();
            play();
        }
        if (userChoice4 == 3) {
            System.out.println("Nothing is here");
            play();
        }
        if (userChoice4 == 4) {
            System.out.println("You have found the note with some weird numbers on it");
            playerChoice();
        }
        if (userChoice4 == 5) {
            System.out.println("The is a padlock key, on the door, you have to insert 3 numbers :");
            insertPassword();
        }
    }
}


class Story {
    static Room1 room = new Room1();

    public static void game() {
        System.out.println("You have woke up in the dark room, you are stunned and you can do below things:");
        room.play();
    }
}